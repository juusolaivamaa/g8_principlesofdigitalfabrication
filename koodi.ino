//Bluetooth
#include "SoftwareSerial.h"
SoftwareSerial SerialBT(0, 1);

//Motor
const int controlPin1 = 14;
const int controlPin2 = 27;
const int enablePin = 33;
int motorSpeed = 0;

//Servo
#include <Servo.h>
Servo servo;
int servoPin = 25;
const int maxSteeringAngle = 180;
const int minSteeringAngle = 0;
int steeringAngle = 90;

void setup() {
  //Bluetooth
  Serial.begin(115200);
  SerialBT.begin("Arduino car");
  Serial.println("Bluetooth Started! Ready to pair...");

  //Motor
  pinMode(controlPin1, OUTPUT);
  pinMode(controlPin2, OUTPUT);
  pinMode(enablePin, OUTPUT);
  digitalWrite(enablePin, LOW);

  //Servo
  servo.attach(servoPin);
  servo.write(steeringAngle);
}

void loop() {
  if (SerialBT.available()) {
    char command = SerialBT.read();
    handleCommand(command);
  }
}

void idle() {
  analogWrite(enablePin, 0); // Disable motor
  steeringAngle = 90;
  servo.write(steeringAngle); // Return steering back to default 90 degrees
}

void forward() {
  // Move forward and return steering back to default
  digitalWrite(controlPin1, HIGH);
  digitalWrite(controlPin2, LOW);
  analogWrite(enablePin, motorSpeed);
  steeringAngle = 90;
  servo.write(steeringAngle);
}

void back() {
  // Move back and return steering back to default
  digitalWrite(controlPin1, LOW);
  digitalWrite(controlPin2, HIGH);
  analogWrite(enablePin, motorSpeed);
  steeringAngle = 90;
  servo.write(steeringAngle);
}

void left() {
  steeringAngle -= 10;
  if (steeringAngle < minSteeringAngle) {
    steeringAngle = minSteeringAngle;
  }
  servo.write(steeringAngle);
}

void right() {
  steeringAngle += 10;
  if (steeringAngle > maxSteeringAngle) {
    steeringAngle = maxSteeringAngle;
  }
  servo.write(steeringAngle);
}

void forwardLeft() {
  //Forward
  digitalWrite(controlPin1, HIGH);
  digitalWrite(controlPin2, LOW);
  analogWrite(enablePin, motorSpeed);
  //Left
  steeringAngle -= 10;
  if (steeringAngle < minSteeringAngle) {
    steeringAngle = minSteeringAngle;
  }
  servo.write(steeringAngle);
}

void forwardRight() {
  //Forward
  digitalWrite(controlPin1, HIGH);
  digitalWrite(controlPin2, LOW);
  analogWrite(enablePin, motorSpeed);
  //Right
  steeringAngle += 10;
  if (steeringAngle > maxSteeringAngle) {
    steeringAngle = maxSteeringAngle;
  }
  servo.write(steeringAngle);
}

void backLeft() {
  //Back
  digitalWrite(controlPin1, LOW);
  digitalWrite(controlPin2, HIGH);
  analogWrite(enablePin, motorSpeed);
  //Left
  steeringAngle -= 10;
  if (steeringAngle < minSteeringAngle) {
    steeringAngle = minSteeringAngle;
  }
  servo.write(steeringAngle);
}

void backRight() {
  //Back
  digitalWrite(controlPin1, LOW);
  digitalWrite(controlPin2, HIGH);
  analogWrite(enablePin, motorSpeed);
  //Right
  steeringAngle += 10;
  if (steeringAngle > maxSteeringAngle) {
    steeringAngle = maxSteeringAngle;
  }
  servo.write(steeringAngle);
}

void setMotorSpeed(char command) {
  if (command == 'q') {
    //Maximum speed
    motorSpeed = 255;
  } else {
    // Calculate speed from numbers 0 - 9
    int speed = (command - '0') * 25;
    motorSpeed = speed;
  }
}

void handleCommand(char command) {  
  switch (command) {
   case 'S':
      idle();
      break;
    case 'F':
      forward();
      break;
    case 'B':
      back();
      break;
    case 'L':
      left();
      break;
    case 'R':
      right();
      break;
    case 'G':
      forwardLeft();
      break;
    case 'I':
      forwardRight();
      break;
    case 'H':
      backLeft();
      break;
    case 'J':
      backRight();
      break;
    case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case 'q':
      setMotorSpeed(command);
      break;
    default:
      break;
    }
}